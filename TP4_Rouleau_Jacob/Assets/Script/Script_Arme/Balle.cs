﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balle : MonoBehaviour
{
    public Rigidbody Physique;
    private float Vitesse;

    public void SetSpeed(float aVitesse) // Va chercher le paramètre de vitesse dans le script de l'arme
    {
        Vitesse = aVitesse; //Dit que la vitesse a la même valeur que la vitesse qu'il va aller chercher dans le script de l'arme
    }

    private void FixedUpdate() // On appelle la physique (fonction)
    {
        Physique.velocity = transform.forward * Vitesse; // Calcul physique que prendra la balle lors de sa projection
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject); // Lorsque la balle entre en collision avec un GameObject, il se détruit
    }
}
