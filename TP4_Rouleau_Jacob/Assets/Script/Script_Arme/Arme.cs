﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arme : MonoBehaviour
{
    public GameObject BallePrefabs;
    public Transform PointApparition;
    public float VitesseBall = 25f;
   // private const float DISTANCE_MAX = 1000f; // En majuscule, car les constante le sont toujours

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            GameObject balle = Instantiate(BallePrefabs, PointApparition.position, PointApparition.rotation); // Création de la balle dans l'arme et dans la bonne rotation
            Balle scriptDeLaBalle = balle.GetComponent<Balle>(); //Il va chercher le script de balle pour l'utiliser

            if (scriptDeLaBalle != null) //Si il ne trouve pas le script, il ne fait rien
            {
                scriptDeLaBalle.SetSpeed(VitesseBall);
            }
        }
        
    }
}
