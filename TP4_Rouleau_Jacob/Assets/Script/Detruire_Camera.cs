﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detruire_Camera : MonoBehaviour
{
 
    //public static void Destroy (IntroCamera obj, float t = 15.0f); //Pour des raisons que j'ignore ceci ne marche pas

    void DestroyObjectDelayed()
    {
        Destroy(gameObject, 15); //Détruire l'objet 15 secondes après son apparition.
    }
}
