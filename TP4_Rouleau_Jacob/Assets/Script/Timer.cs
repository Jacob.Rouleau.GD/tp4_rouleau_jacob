﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text TimerText; 
    private float startTime;

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time; 
    }

    // Update is called once per frame
    void Update()
    {
        float t = Time.time - startTime;    //Ça nous donne un le temps depuis que le timer à commencé

        string minutes = ((int) t / 60).ToString(); // On transforme la donnée numérique en lettres (minutes)
        string seconds = (t % 60).ToString("f3"); // Même chose, mais ici je ne veux que trois float (Donc trois décimales)

        TimerText.text = minutes + ":" + seconds; 
    }
}