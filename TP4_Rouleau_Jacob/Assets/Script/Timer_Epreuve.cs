﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer_Epreuve : MonoBehaviour
{
  public bool StartTimer = false;
  private float time = 0f;
  public Text TimerOnScreen;

  void OnTriggerEnter(Collider other)
  {
      if (other.tag == "TimerStart")
      {
        
              StartTimer = true; 
              time = 0f; 
         
      }
      else if (other.tag == "TimerEnd")
      {
          if (StartTimer)
          {
              StartTimer = false;
          }
      }
  }

void Update()
{
    if (StartTimer)
    {
        TimerOnScreen.color = Color.green;
        time += Time.deltaTime;
        TimerOnScreen.text = time.ToString();

    }
    
    else
    {
       TimerOnScreen.color = Color.red;
    }
}

}
