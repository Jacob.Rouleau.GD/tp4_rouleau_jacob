﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Apparition : MonoBehaviour
{
    public GameObject Boss;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Instantiate(Boss, Boss.transform.position, Quaternion.identity);
        }
    }
}
